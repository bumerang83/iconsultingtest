package iconsultingtest;

import iconsultingtest.service.SearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.Banner;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import static java.lang.System.exit;

// https://www.mkyong.com/spring-boot/spring-boot-non-web-application-example/
@SpringBootApplication
public class IconsultingtestApplication implements CommandLineRunner {

    @Autowired
    private SearchService searchService;

    public static void main(String[] args) throws Exception {

        //disabled banner, don't want to see the spring logo
        SpringApplication app = new SpringApplication(IconsultingtestApplication.class);
        app.setBannerMode(Banner.Mode.OFF);
        app.run(args);

    }

    // Put your logic here.
    @Override
    public void run(String... args) throws Exception {
//        String[] argsTest = new String[]{"-f", "test-files.xml"};
//        String[] argsTest = new String[]{"-f", "test-files.xml", "-s", "file-1498940214.xhtml"};
        //*[matches(@id, 'sometext\d+_text')]
        //String[] argsTest = new String[]{"-f", "test-files.xml", "-s", "file-847675734.xhtml"};
        //String[] argsTest = new String[]{"-f", "test-files.xml", "-s", "//*[matches(@id, 'sometext\\d+_text')]"};
//        String[] argsTest = new String[]{"-f", "test-files.xml", "-s", "*.java"};
//        String[] argsTest = new String[]{"-f", "test-files.xml", "-s", "*.xhtml"};
        //String[] argsTest = new String[]{"-f", "test-files.xml", "-S", ".*?[a-z]{4}-\\\\d+\\.[a-z]+"};
//        String[] argsTest = new String[]{"-f", "test-files.xml", "-S", ".*?[a-z]{4}-\\d+\\.[a-z]+"};//{4}-d+.[a-z]+"};
        //String[] argsTest = new String[]{"-f", "test-files.xml", "-S", "[a-z]{4}-\\d+.[a-z]+"};//{4}-d+.[a-z]+"};

//        this.serachService = new SerachService(argsTest);
//        System.exit(0);
        if (args.length == 2 || args.length == 4) {
//            System.out.println(helloService.getMessage(args[0].toString()));
            System.out.println("The search service " + this.searchService.toString());

            this.searchService = new SearchService(args);
            this.searchService.process();

            /*
            System.out.println("The CL arguments:");
            for (int i = 0; i < args.length; i++) {
                System.out.println(i + "->" + args[i]);
            }*/

//            System.out.println("-f from the service: " + this.serachService.getF());
//            System.out.println("-xml_file from the service: " + this.serachService.getXmlFile());
//            System.out.println("-s from the service: " + this.serachService.getS());
//            System.out.println("-input from the service: " + this.serachService.getInput());
        } else {
            this.searchService.process();
            this.searchService.formatNotice();
        }
        exit(0);
    }
}
