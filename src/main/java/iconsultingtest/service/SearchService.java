package iconsultingtest.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Source;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import javax.xml.xpath.*;
import java.io.*;

@Service
public class SearchService {
    private static final String XSD_SCHEMA = "./target/classes/RECOURSIVE_XSD_FINAL.xsd";
    private static final String PATTERN = "//*/children/child[@is-file='true']/name/text()";

    @Value("${f:#{null}}")
    private String f;
    @Value("${xmlFile:#{null}}")
    private String xmlFile;
    @Value("${s:#{null}}")
    private String s;
    @Value("${input:#{null}}")
    private String input;


    public SearchService() {
    }

    public SearchService(String[] args) {
        if (args.length == 4) {
            this.setFourArgs(args);
        } else if (args.length == 2) {
            this.setTwoArgs(args);
        }
    }

    public void process() {
        try {
            if (this.getS() == null) {
                if (this.validateXml(this.getXmlFile(), this.XSD_SCHEMA)) {
//                    System.out.println("Parsing file -> " + this.getXmlFile());
                    NodeList resultList = searchData(this.getXmlFile(), PATTERN);
                    this.printResult(resultList, null);
                }
            } else if (this.validateXml(this.getXmlFile(), this.XSD_SCHEMA)) {
                NodeList resultList = searchData(this.getXmlFile(), PATTERN);
                this.printResult(resultList, this.getInput());
            }
            else {
                this.errorValidation();
            }
        } catch(Exception e) {
            this.fileNotFound();
        }
    }

    private void printResult(NodeList nodeList, String searchPattern) {
        if (searchPattern == null) {
            for (int i = 0; i < nodeList.getLength(); i++) {
                System.out.println(this.getXPath(nodeList.item(i)));
            }
        } else {
            String sPattern = null;
            if (this.getS().equals("-s")) {
                if (this.getInput().contains("*.")) {
                    sPattern = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+" + searchPattern.substring(1, searchPattern.length()) + "$";
                } else {
                    sPattern = "^" + searchPattern + "$";
                }
            } else {
                sPattern = "^" + searchPattern + "$";
            }
            for (int i = 0; i < nodeList.getLength(); i++) {
                java.util.regex.Pattern p = java.util.regex.Pattern.compile(sPattern);
                java.util.regex.Matcher m = p.matcher(nodeList.item(i).getTextContent().trim());
                if (m.matches()) {
                    System.out.println(this.getXPath(nodeList.item(i)));
                }
            }
        }
    }

    // https://stackoverflow.com/questions/32716850/how-to-use-saxon-xpath-2-0-with-java
/*
    private void go(String filename, String xPathExpressionStr, String objectModel) throws Exception {
        System.out.println("Loading classes, parsing '" + filename + "', and setting up serializer.");
        System.setProperty("javax.xml.xpath.XPathFactory:" + NamespaceConstant.OBJECT_MODEL_SAXON, "net.sf.saxon.xpath.XPathFactoryImpl");
        XPathFactory xPathFactory = XPathFactory.newInstance(NamespaceConstant.OBJECT_MODEL_SAXON);
        XPath xPath = xPathFactory.newXPath();
//        InputSource inputSource = new InputSource(new File("simple.xml").toURI().toString());
        InputSource inputSource = new InputSource(new ClassPathResource(filename).getFile().toURI().toString());
        SAXSource saxSource = new SAXSource(inputSource);
        Configuration config = ((XPathFactoryImpl) xPathFactory).getConfiguration();
        DocumentInfo document = config.buildDocument(saxSource);
//        String xPathStatement = "//firstname";
        String patternStr = "^[a-z]*$";
        Pattern pattern = Pattern.compile(patternStr);
//        String xPathStatement = xPathExpressionStr;
        String xPathStatement = patternStr;
        XPathExpression xPathExpression = xPath.compile(xPathStatement);
        List matches = (List) xPathExpression.evaluate(document, XPathConstants.NODESET);
//        NodeList matches = (NodeList) xPathExpression.evaluate(document, XPathConstants.NODESET);

        if (matches != null) {
            for (Iterator iter = matches.iterator(); iter.hasNext(); ) {
                NodeInfo node = (NodeInfo) iter.next();
                System.out.println("Node ->" + node.toString());
//                assertEquals("firstname", node.getDisplayName());
//                assertEquals("James", node.getStringValue());
            }
        }

///////////////////////////////////////////////////////////////////////////////////////////////////////////
        Processor processor = new Processor(false);
//        XdmNode xdm = processor.newDocumentBuilder().build(new StreamSource(new StringReader(filename)));
        XdmNode xdm = processor.newDocumentBuilder().build(new ClassPathResource(filename).getFile());
        XdmValue result = processor.newXPathCompiler().evaluate(xPathExpressionStr, xdm);

        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < result.size(); i++) {
            sb.append(result.itemAt(i).getStringValue());
            if (i + 1 != result.size()) {
                sb.append('\n');
            }
        }
    }
    */

    private void setTwoArgs(String[] args) {
        if (args[0].equals("-f")) {
            this.setF(args[0]);
            this.setXmlFile("./target/classes/" + args[1]);
        } else {
            this.formatNotice();
        }
    }

    private void setFourArgs(String[] args) {
        if (args[0].equals("-f")) {
            this.setF(args[0]);
            this.setXmlFile("./target/classes/" + args[1]);
            this.setS(args[2]);
            this.setInput(args[3]);
        } else if (args[0].equalsIgnoreCase("-s")) {
            this.setS(args[0]);
            this.setInput(args[1]);
            this.setF(args[2]);
            this.setXmlFile("./target/classes/" + args[3]);
        } else {
            this.formatNotice();
        }
    }

    // https://stackoverflow.com/questions/15732/whats-the-best-way-to-validate-an-xml-file-against-an-xsd-file
    private boolean validateXml(String xmlFile, String xsdSchema) {
        try {
            // parse an XML document into a DOM tree
            DocumentBuilder parser = DocumentBuilderFactory.newInstance().newDocumentBuilder();
//            System.out.println("Parser file as argument -> " + xmlFile);
//            System.out.println("Parser file as resource -> " + new ClassPathResource(xmlFile).getFile().toString());
//            Document document = parser.parse(new ClassPathResource(xmlFile).getFile());
            Document document = parser.parse(xmlFile);
//            Document document = parser.parse(this.getClass().getResourceAsStream(xmlFile));
//            is = this.getClass().getResourceAsStream(templateFilePath)
//            System.out.println("After the parser file -> " + xmlFile);

            // create a SchemaFactory capable of understanding WXS schemas
            SchemaFactory factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);

            // load a WXS schema, represented by a Schema instance
//            Source schemaFile = new StreamSource(new ClassPathResource(xsdSchema).getFile());
            Source schemaFile = new StreamSource(xsdSchema);
            Schema schema = factory.newSchema(schemaFile);

            // create a Validator instance, which can be used to validate an instance document
            Validator validator = schema.newValidator();
            validator.validate(new DOMSource(document));
//            System.out.println("After the validation");
            return true;

        } catch (SAXException | IOException | ParserConfigurationException e) {
            this.errorValidation();
            return false;
        }
    }

    private NodeList searchData(String xmlFile, String pattern) {
        try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            dbFactory.setNamespaceAware(true); // never forget this!
            DocumentBuilder dBuilder;
            dBuilder = dbFactory.newDocumentBuilder();
//            Document doc = dBuilder.parse(new ClassPathResource(xmlFile).getFile());
            Document doc = dBuilder.parse(xmlFile);
            doc.getDocumentElement().normalize();
            XPath xPath = XPathFactory.newInstance().newXPath();
            NodeList nodeListChild = (NodeList) xPath.compile(pattern).evaluate(
                    doc, XPathConstants.NODESET);
            return nodeListChild;
        } catch (Exception e) {
            this.parsingError();
        }
        return null;
    }

    private String getXPath(Node node) {
        Node parent = node.getParentNode();
        if (parent == null) {
            return "";
        }
        if (parent.getNodeName().equals("name")) {
            String path = getXPath(parent).trim();
            if (path.equals("/")) {
                return getXPath(parent) + node.getNodeValue();
            } else {
                return getXPath(parent) + "/" + node.getNodeValue();
            }
        } else {
            Node sibling = parent.getPreviousSibling();
            if (sibling != null) {
                Node previousSibling = sibling.getPreviousSibling();
                if (previousSibling != null && previousSibling.getNodeName().equals("name")) {
                    Node child = previousSibling.getFirstChild();
                    String path = getXPath(parent).trim();
                    if (path.equals("") || path.equals("/")) {
                        return path + child.getNodeValue();
                    }
                    return path + "/" + child.getNodeValue();
                }
            }
        }
        return getXPath(parent);
    }

    public String getF() {
        return f;
    }

    public void setF(String f) {
        this.f = f;
    }

    public String getXmlFile() {
        return xmlFile;
    }

    public void setXmlFile(String xmlFile) {
        this.xmlFile = xmlFile;
    }

    public String getS() {
        return s;
    }

    public void setS(String s) {
        this.s = s;
    }

    public String getInput() {
        return input;
    }

    public void setInput(String input) {
        this.input = input;
    }

    public void formatNotice() {
        System.out.println("#################################################################");
        System.out.println("The wrong format for calling the application was used!");
        System.out.println("The right formats are: ");
        System.out.println("'java -jar ./out/artifacts/iconsultingtest_jar/iconsultingtest.jar -f <xml_file>");
        System.out.println("'java -jar ./out/artifacts/iconsultingtest_jar/iconsultingtest.jar -f <xml_file> -s <inputFileName>'");
        System.out.println("'java -jar ./out/artifacts/iconsultingtest_jar/iconsultingtest.jar -f <xml_file> -s <*.fileExtension>'");
        System.out.println("'java -jar ./out/artifacts/iconsultingtest_jar/iconsultingtest.jar -s <*.fileExtension> -f <xml_file>'");
        System.out.println("'java -jar ./out/artifacts/iconsultingtest_jar/iconsultingtest.jar -f <xml_file> -S <regexp>'");
        System.out.println("'java -jar ./out/artifacts/iconsultingtest_jar/iconsultingtest.jar -S <regexp> -f <xml_file>'");
        System.out.println("");
        System.out.println("So used default values for the application 'java -jar iconsultingtest.jar " + this.getF() + " " + this.getXmlFile() + " " + this.getS() + " " + this.getInput() + "'");
        System.out.println("#################################################################");
    }

    public void errorValidation() {
        System.err.println("An error occured while the validation process! Check the input arguments!");
    }

    public void fileNotFound() {
        System.err.println("The required file(s) not found!");
    }

    public void parsingError() {
        System.err.println("Error occured while parsing!");
    }
}