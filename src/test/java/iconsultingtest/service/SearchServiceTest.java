package iconsultingtest.service;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class SearchServiceTest {

    private SearchService searchService;

    @Before
    public void setUp() {
        this.searchService = new SearchService(new String[]{"-f", "test-files.xml", "-s", "file-1498940214.xhtml"});
    }

    @After
    public void tearDown() {
        this.searchService = null;
    }

    @Test
    public void processTest() {
        System.out.println("The process method test 'processTest'");
        this.searchService.process();
    }

    @Test
    public void getFTest() {
        String result = "-f";
        assertEquals(this.searchService.getF(), result);
    }

    @Test
    public void setFTest() {
        String result = "-f";
        this.searchService.setF(result);
        assertEquals(this.searchService.getF(), result);
    }

    @Test
    public void getXmlFileTest() {
        String result = "test-files.xml";
        assertEquals(this.searchService.getXmlFile(), result);
    }

    @Test
    public void setXmlFileTest() {
        String result = "test-files.xml";
        this.searchService.setXmlFile(result);
        assertEquals(this.searchService.getXmlFile(), result);
    }

    @Test
    public void getSTest() {
        String result = "-s";
        assertEquals(this.searchService.getS(), result);
    }

    @Test
    public void setSTest() {
        String result = "-s";
        this.searchService.setS(result);
        assertEquals(this.searchService.getS(), result);
    }

    @Test
    public void getInputTest() {
        String result = "file-1498940214.xhtml";
        this.searchService.setInput(result);
        assertEquals(this.searchService.getInput(), result);
    }

    @Test
    public void setInput() {
        String result = "file-1498940214.xhtml";
        this.searchService.setInput(result);
        assertEquals(this.searchService.getInput(), result);
    }

    @Test
    public void formatNotice() {
        this.searchService.formatNotice();
    }
}